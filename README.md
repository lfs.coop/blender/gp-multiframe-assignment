# Grease Pencil Multiframe Assignment

Assign a material to a stroke in a range of frames in one click.
Select only one reference stroke, and the tool will automatically find a corresponding stroke in each frame of a range you specify, within the same layer and the same material as your reference stroke.
You can also select a bunch of strokes in the same frame, and the process will be applied for each of them.

## Usage

![Tool Overview](doc/multiframe-assign.png "Overview of the interface of the tool")

In Edit Mode, with a grease pencil object active, select one or more stroke to which you want to assign the same material. Right-click in the viewport to open the context menu, and select 'Assign Material (multiple frames)'. Then, you can chose the material to assign to the strokes, and the frame range to look for corresponding strokes. 
You can also specify a distance threshold above which strokes won't be considered for the pairing.

## Distance specification
We measure the distance between two stroke paths using the Hausdorff distance, which computes the maximum of the euclidian distance between each point of a path to the other. 
This computation was implemented in a naive way, and can take some time. 

Plus, you might want to use another distance measure to improve your results. To do so, simply change the variable `distance_measure` in `sma_utils.py` to any function that takes as input two list of Vector, and returns a positive real number.

## License

Published under GPLv3 license.


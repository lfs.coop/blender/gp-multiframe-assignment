import bpy
from bpy.props import *
from . sma_utils import *
from time import time

class STKMULTIASSIGN_OT_multiAssignMaterial(bpy.types.Operator):
    bl_idname = "gpencil.stroke_multi_assign_material"
    bl_label = "Multi Assign Material"
    bl_description = "Assign a material to a temporally consistent set of strokes"
    bl_options= {'UNDO'}

    ''' Frame range should always remain sane : start < end 
        That is what the update functions are for
    '''
    def update_frame_start(self, context):
        if self.frame_start > self.frame_end:
            self.frame_start = self.frame_end
        
    def update_frame_end(self, context):
        if self.frame_start > self.frame_end:
            self.frame_end = self.frame_start

    frame_start : IntProperty(name='Frame start', description='First frame on which to apply the assignment' , update=update_frame_start)
    frame_end : IntProperty(name='Frame end', description='Last frame on which to apply the assignment', update=update_frame_end)
    source_material: StringProperty(name='Source Material', description='Material of the source stroke')
    target_material: StringProperty(name='Material', description='Material to assign to strokes')
    distance_threshold: FloatProperty(name='Distance threshold', description='Maximal distance to make strokes correspond', default=0.5, min=0)

    record_time: BoolProperty(name='Record execution time (debug)', default=True)

    def execute(self, context):   
        if self.record_time:
            tstmp = time()

        ob = context.active_object  
        wm = context.window_manager
        frs, fre = self.frame_start, self.frame_end
        frnb = fre-frs+1

        mat_index = get_material_index(ob, self.target_material)
        if mat_index < 0:            
            # Note : this should not happen since the UI shows the list of materials of the object
            self.report({'ERROR'}, f'Could not find material {self.target_material}')
            return {'CANCELLED'}
        
        strokes = get_selected_strokes(ob)
        if strokes is None:
            self.report({'INFO'}, 'No strokes selected')
            return {'CANCELLED'}   

        wm.progress_begin(0, len(strokes)*frnb)  
        progress = 0

        ''' For each selected stroke, 
            get the similar stroke in each frame and assign material 
        '''
        for lay_name, stk in strokes:
            lay = ob.data.layers[lay_name]
            sim_stk = get_similar_strokes(lay, stk, frs, fre, self.distance_threshold, wm, progress)
            progress += frnb
            for stk in sim_stk:
                stk.material_index = mat_index

        wm.progress_end()
        
        if self.record_time:
            tm = time() - tstmp
            print(f'Time : {tm}s ({len(strokes)} strokes on {fre-frs} frames)')

        return {'FINISHED'}

    def draw(self, context):
        ob = context.active_object.data        

        layout = self.layout

        row = layout.row()
        row.prop_search(self, 'target_material', ob, 'materials')

        row = layout.row(align=True)
        row.label(text='Frame range')
        row.prop(self, 'frame_start', text='Start')
        row.prop(self, 'frame_end', text='End')
    
        row = layout.row(align=True)
        row.prop(self, 'distance_threshold')

    def invoke(self, context, event):
        self.frame_start = context.scene.frame_start
        self.frame_end = context.scene.frame_end      
        return context.window_manager.invoke_props_dialog(self)
    


class STKMULTIASSIGN_OT_multiFrameSelection(bpy.types.Operator):
    bl_idname = "gpencil.multi_frame_stroke_select"
    bl_label = "Multi-frame stroke select"
    bl_description = "Select a consistent set of strokes through neighbouring frames"
    bl_options= {'UNDO'}

    ''' Frame range should always remain sane : start < end 
        That is what the update functions are for
    '''
    def update_frame_start(self, context):
        if self.frame_start > self.frame_end:
            self.frame_start = self.frame_end
        
    def update_frame_end(self, context):
        if self.frame_start > self.frame_end:
            self.frame_end = self.frame_start

    frame_start : IntProperty(name='Frame start', description='First frame on which to apply the assignment' , update=update_frame_start)
    frame_end : IntProperty(name='Frame end', description='Last frame on which to apply the assignment', update=update_frame_end)
    distance_threshold: FloatProperty(name='Distance threshold', description='Maximal distance to make strokes correspond', default=0.5, min=0)

    record_time: BoolProperty(name='Record execution time (debug)', default=False)

    def execute(self, context):   
        if self.record_time:
            tstmp = time()

        ob = context.active_object  
        wm = context.window_manager
        frs, fre = self.frame_start, self.frame_end
        frnb = fre-frs+1
 
        strokes = get_selected_strokes(ob)
        if strokes is None:
            self.report({'INFO'}, 'No strokes selected')
            return {'CANCELLED'}   

        wm.progress_begin(0, len(strokes)*frnb)  
        progress = 0

        for lay_name, stk in strokes:
            lay = ob.data.layers[lay_name]
            sim_stk = get_similar_strokes(lay, stk, frs, fre, self.distance_threshold, wm, progress)
            progress += frnb
            for stk in sim_stk:
                stk.select = True

        wm.progress_end()
        
        if self.record_time:
            tm = time() - tstmp
            print(f'Time : {tm}s ({len(strokes)} strokes on {fre-frs} frames)')

        return {'FINISHED'}

    def draw(self, context):
        ob = context.active_object.data        

        layout = self.layout

        row = layout.row()
        row.prop_search(self, 'target_material', ob, 'materials')

        row = layout.row(align=True)
        row.label(text='Frame range')
        row.prop(self, 'frame_start', text='Start')
        row.prop(self, 'frame_end', text='End')
    
        row = layout.row(align=True)
        row.prop(self, 'distance_threshold')

    def invoke(self, context, event):
        self.frame_start = context.scene.frame_start
        self.frame_end = context.scene.frame_end      
        return context.window_manager.invoke_props_dialog(self)

def menu_func(self, context):
    self.layout.operator_context = 'INVOKE_DEFAULT'
    self.layout.operator("gpencil.stroke_multi_assign_material", \
        text="Assign Material (multiple frames)")
    self.layout.operator("gpencil.multi_frame_stroke_select", \
        text="Select Similar Strokes")

classes = [STKMULTIASSIGN_OT_multiAssignMaterial, STKMULTIASSIGN_OT_multiFrameSelection]

def register():
    for cls in classes:
        bpy.utils.register_class(cls) 

    bpy.types.VIEW3D_MT_gpencil_edit_context_menu.append(menu_func)

def unregister():   
    bpy.types.VIEW3D_MT_gpencil_edit_context_menu.remove(menu_func)

    for cls in reversed(classes):
        bpy.utils.unregister_class(cls)
import bpy, os
from . sma_maths import hausdorff_distance, discrete_second_derivative
from bpy.path import abspath

def get_material_index(ob, mat_name):
    ob_mats = ob.material_slots.keys()
    try:
        return ob_mats.index(mat_name)
    except ValueError:
        return -1

def get_selected_strokes(ob):
    if (ob is None) or (ob.type != 'GPENCIL'):
        return []
    return [(lay.info, stk) for lay in ob.data.layers if (not lay.hide) and (not lay.lock) and (lay.active_frame) \
                            for stk in lay.active_frame.strokes if stk.select]

def get_similar_strokes(layer, stroke, frame_start, frame_end, dst_threshold=0.1, window_manager=None, initial_progress=-1):
    distance_measure = hausdorff_distance

    def frame_in_range(f):
        nonlocal frame_start, frame_end
        return (f.frame_number >= frame_start) \
            and (f.frame_number <= frame_end)

    def strokes_distance(stroke_0, stroke_1):
        nonlocal distance_measure
        co_0 = [p.co for p in stroke_0.points]
        co_1 = [p.co for p in stroke_1.points]

        # d2d_0 = discrete_second_derivative(co_0)
        # d2d_1 = discrete_second_derivative(co_1)

        return distance_measure(co_0, co_1)
    
    def strokes_compatible(stroke_0, stroke_1):
        return (len(stroke_0.points) > 0) \
            and (stroke_0.material_index == stroke_1.material_index)
    
    sim_strokes = []

    def get_similar_in_frame(frame, prev_s):
        nonlocal stroke, dst_threshold

        d_stk = [ (strokes_distance(prev_s, s),i) \
                  for i,s in enumerate(frame.strokes) \
                  if strokes_compatible(s,stroke)]
        if not d_stk:
            return prev_s
        (min_dst, min_ind) = min(d_stk)

        if min_dst > dst_threshold:
            return prev_s
        sim_stk = frame.strokes[min_ind]
        sim_strokes.append((frame.frame_number, min_ind, sim_stk))

        return sim_stk        

    total_frames = frame_end - frame_start + 1
    progress = initial_progress

    def incr_progress(fnb):
        nonlocal window_manager, progress, total_frames
        if (window_manager is None):
            return
        progress += 1
        window_manager.progress_update(progress)

    stroke_frame = layer.active_frame.frame_number

    ''' Dealing with frames that are before the reference in timeline '''
    frames_before = [(f.frame_number,f) for f in layer.frames if (f.frame_number < stroke_frame) and frame_in_range(f) ]
    prev_s = stroke 
    fnb = 0
    for fnb,frame in sorted(frames_before, reverse=True):
        prev_s = get_similar_in_frame(frame, prev_s)
        incr_progress(fnb)
    
    ''' Adding the reference frame itself '''
    if frame_in_range(layer.active_frame):
        get_similar_in_frame(layer.active_frame, stroke)
        incr_progress(fnb)
    
    ''' Dealing with frames that are after the reference in timeline '''
    frames_after = [(f.frame_number,f) for f in layer.frames if (f.frame_number > stroke_frame) and frame_in_range(f) ]
    prev_s = stroke 
    for fnb,frame in sorted(frames_after, reverse=False):
        prev_s = get_similar_in_frame(frame, prev_s)
        incr_progress(fnb)

    return [stk for fnb, sid, stk in sorted(sim_strokes) ]
    


def euclidian_distance(a,b):
    return (a-b).length

def hausdorff_distance_(A, B, dist_function = euclidian_distance):
    return max([ min([dist_function(a,b) for b in B]) for a in A ])

def hausdorff_distance(A, B, dist_function = euclidian_distance):
    if(len(A) == 0):
        return 0 if (len(B) == 0) else 1e5
    if(len(B) == 0):
        return 1e5 
    return max(hausdorff_distance_(A, B, dist_function),hausdorff_distance_(A, B, dist_function))

def sanitize_path(path):
    return [path[0]] + [ p for i,p in enumerate(path[1:]) if (path[i] - path[i+1]).length > 1e-5 ]

def discrete_second_derivative(path):
    spath = sanitize_path(path)

    def second_derivative_at(i, p):
        p_left = spath[i-1]
        p_right = spath[i+1]

        h_left = (p_left - p).length
        h_right = (p_right - p).length

        return (p_right - 2*p + p_left)/(h_left*h_right)

    path_second_derivative = [ second_derivative_at(i,p) for i,p in enumerate(spath) if (i > 0) and (i < len(spath)-1)]

    return path_second_derivative

        